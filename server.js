//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var bodyParser =require('body-parser') ;
app.use(bodyParser.json());

app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With, Content-Type, Accept");
  next();
});
var requestJson = require('request-json');
var urlMovimientosMLab = "https://api.mlab.com/api/1/databases/bdbanca4mb79560/collections/movimientos?apiKey=j7EFU8TsmOhd8S4Ncwx7e7rpLMjku1IP";
var clienteMLab = requestJson.createClient(urlMovimientosMLab);

requestJson = require('request-json');
var urlUsuariosMLab = "https://api.mlab.com/api/1/databases/bdbanca4mb79560/collections/usuarios?apiKey=j7EFU8TsmOhd8S4Ncwx7e7rpLMjku1IP";
//var usuariosMLab = requestJson.createClient(urlUsuariosMLab);
var path = require('path');

app.listen(port);


console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
  //res.send('Hola Mundo!!');
  //__dirname incica que lo tome del proyecto donde estoy parado
  res.sendFile(path.join(__dirname,'index.html'));
});
app.get("/clientes/:idCliente", function(req, res){
  res.send('Aquì tiene al cliente número :' + req.params.idCliente);
});

app.post("/",function(req, res){
  res.send('Hemos recibido su petición cambiada');
});
app.put("/",function(req, res){
  res.send('Hemos recibido su petición');
});

app.get("/movimientos", function(req, res){
  clienteMLab.get ('',function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  });
});

app.get("/clienteMaximo", function(req, res){
  var filtros = '&s={"idCliente":-1} &l=1';
  var proyeccion = '&f={"idCliente":1, "_id":0}';
  var urlMovimientosMLabQry = urlMovimientosMLab + proyeccion+filtros;
  var clienteMLabQry = requestJson.createClient(urlMovimientosMLabQry);

  clienteMLabQry.get ('',function(err, resM, body){
  //  clienteMLab.get ('',function(err, resM, body){
    if(err){
      console.log(body);
      console.log(err);
      }else{
        console.log("body "+ body);
        console.log(JSON.stringify(body));
        res.send(resM.body);
    }
  });
});

app.post("/clienteNvo",function(req, res){
  console.log("body req " + req.body);
  clienteMLab.post('', req.body, function(err, resM, body){
    console.log("body recibido:" + JSON.stringify(req.body) );
    console.log("body de respuesta:"+ JSON.stringify(body) );
    console.log("alta cliente ok" + resM.body)

    if(JSON.stringify(body).length <=2 ){
 //   if(bodyArray[0].idCliente ){
      console.log("Hubo un error al ejecutar el alta del cliente Por favor verifique todos los datos o intente mas tarde. "+req.body);
      res.send(JSON.stringify({"statusCode":400, "description":"Hubo un error al ejecutar el alta del cliente Por favor verifique todos los datos o intente mas tarde. " }));
    }else{
      console.log("usuario dado de alta OK" +JSON.stringify(body));
      //el codigo de alta ok debe ser 201
      res.send( JSON.stringify({"statusCode":201, "description":"Cliente dado de alta satisfactoriamente" }));
    }
  });
});

app.post('/usuario',function(req,res) {

     var filtros = '&q={"idCliente":'+req.body.idCliente +'}';
     var urlMovimientosMLabQry = urlMovimientosMLab + filtros;
     var clienteMLabQry = requestJson.createClient(urlMovimientosMLabQry);
     console.log(urlMovimientosMLabQry);

     clienteMLabQry.get ('',function(err, resM, body){
      console.log("body recibido:" + JSON.stringify(req.body) );
      console.log("body de respuesta:"+ JSON.stringify(body) );
    //  console.log("body de respuesta resM:"+ JSON.stringify(resM.body) );
      var bodyArray = JSON.stringify(body);
      console.log("body length: "+ JSON.stringify(body).length);
      console.log("err: "+ err);
      console.log(resM.statusCode);

       if(err){
         console.log("error body"+resM.body);
         res.send( JSON.stringify({"statusCode":500, "description":"Eror en la conexion con el servidro de Base de datos" }));
       }else{
         if(JSON.stringify(body).length <= 2  || resM.statusCode !=200 ){
      //   if(bodyArray[0].idCliente ){
           console.log("idCliente no encontrado "+req.body.idCliente);
           res.send(JSON.stringify({"statusCode":400, "description":"Ingresa un id de Cliente valido." }));
         }else{
           var nvoUsuarioMLab = requestJson.createClient(urlUsuariosMLab);

           nvoUsuarioMLab.post('',req.body, function(err, resT, body){
           console.log("usuario dado de alta OK" +JSON.stringify(body));
           //el codigo de alta ok debe ser 201
           res.send( JSON.stringify({"statusCode":201, "description":"Usuario dado de alta satisfactoriamente" }));
          // res.send(body);
     });
   }
 }
 });
 });

app.get("/usuarios", function(req, res){
  var fields = '&f={"idCliente": 1, "nombre": 1,"apellidoPaterno": 1, "apellidoMaterno":1, "email": 1}';
  console.log ("fields: "+fields);
  urlUsuariosMLabQ= urlUsuariosMLab+fields;
  var usuariosMLab = requestJson.createClient(urlUsuariosMLabQ);
  usuariosMLab.get ('',function(err, resM, body){
  console.log("get usuarios usuarios body.correo: "+ JSON.stringify(body.correo));
   console.log("password: "+  JSON.stringify(req.body.password));
   console.log("body:" + JSON.stringify(req.body));
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  });
});

/*
app.post("/usuarioLogin",function(req, res){

  usuariosMLab.get('', req.body, function(err, resM, body){
var resultado = 1;
console.log("usuario Login post ::correo: "+ req.body.correo);
console.log("password: "+ JSON.stringify(req.body.password));
console.log("body:" +body);
    res.send(body);
  });
}); */


app.post("/usuarios/login",function(req, res) {

    console.log("correo: "+ req.body.correo);
    console.log("password: "+ req.body.password);

    if(!req.body.correo || !req.body.password)
    {
      res.send( JSON.stringify({"statusCode":400, "error":"El correo y el password no pueden ir vacíos" }));
    }else {

         var correo= req.body.correo;
         var password= req.body.password;
         var filtros = '&q={"email":"'+correo+'","password":"'+password+'"}';

         console.log ("filtros: "+filtros);
         urlUsuariosMLabQ= urlUsuariosMLab+filtros;
        var usuariosMLab = requestJson.createClient(urlUsuariosMLabQ);

            usuariosMLab.get('', function(err, resM, body){
                           console.log("body length: "+JSON.stringify(resM.body.length));
                           console.log("resM"+resM);
                           console.log("resM.body"+resM.body);
                           console.log("body"+body);
                           console.log("statusCode " + resM.statusCode);

                console.log ("respuesta de busqueda de usuario: " + resM.body );
                correo= resM.body.correo;
                password= resM.body.password;
            //    res.send(JSON.stringify(respuesta));
                 if( correo && password && resM.statusCode){
                   res.send( JSON.stringify({"statusCode":resM.statusCode ,"body":resM.body}));
                  } else {
                    console.log("statusCode: "+ resM.statusCode +" error : "+err);
                  res.send( JSON.stringify({"statusCode":204, "description":err }));

                            //  console.log(JSON.stringify(body[0]));

                  }
              });
    }
  });



  if(process.env.NODE_ENV !== 'production') {
    process.once('uncaughtException', function(err) {
      console.error('FATAL: Uncaught exception.');
      console.error(err.stack||err);
      setTimeout(function(){
        process.exit(1);
      }, 100);
    });
  }
